// Load required modules
var http    = require("http");              // http server core module
var express = require("express");           // web framework external module
var serveStatic = require('serve-static');  // serve static files
var socketIo = require("socket.io");        // web socket external module
var { Pool, Client } = require('pg');
var process_runner = require('./fire_crawler_proccess/crawler_process');


// This sample is using the easyrtc from parent folder.
// To use this server_example folder only without parent folder:
// 1. you need to replace this "require("../");" by "require("easyrtc");"
// 2. install easyrtc (npm i easyrtc --save) in server_example/package.json

var easyrtc = require("easyrtc"); // EasyRTC internal module

// Set process name
process.title = "node-easyrtc";

// Setup and configure Express http server. Expect a subfolder called "static" to be the web root.
var app = express();
app.use(serveStatic('static', {'index': ['index.html']}));



// Start Express http server on port 8080
var webServer = http.createServer(app);

// Start Socket.io so it attaches itself to Express server
var socketServer = socketIo.listen(webServer, {"log level":1});

easyrtc.setOption("logLevel", "debug");

// Overriding the default easyrtcAuth listener, only so we can directly access its callback
easyrtc.events.on("easyrtcAuth", function(socket, easyrtcid, msg, socketCallback, callback) {
    easyrtc.events.defaultListeners.easyrtcAuth(socket, easyrtcid, msg, socketCallback, function(err, connectionObj){
        if (err || !msg.msgData || !msg.msgData.credential || !connectionObj) {
            callback(err, connectionObj);
            return;
        }

        connectionObj.setField("credential", msg.msgData.credential, {"isShared":false});

        console.log("["+easyrtcid+"] Credential saved!", connectionObj.getFieldValueSync("credential"));

        callback(err, connectionObj);
    });
});

// To test, lets print the credential to the console for every room join!
easyrtc.events.on("roomJoin", function(connectionObj, roomName, roomParameter, callback) {
    console.log("["+connectionObj.getEasyrtcid()+"] Credential retrieved!", connectionObj.getFieldValueSync("credential"));
    easyrtc.events.defaultListeners.roomJoin(connectionObj, roomName, roomParameter, callback);
});

// Start EasyRTC server
var rtc = easyrtc.listen(app, socketServer, null, function(err, rtcRef) {
    console.log("Initiated");

    rtcRef.events.on("roomCreate", function(appObj, creatorConnectionObj, roomName, roomOptions, callback) {
        console.log("roomCreate fired! Trying to create: " + roomName);

        appObj.events.defaultListeners.roomCreate(appObj, creatorConnectionObj, roomName, roomOptions, callback);
    });
});

/** Fire the crawlers as processes **/
process_runner.fire_crawlers();
/** End (fire the crawlers as processes. **/


//listen on port 8080
webServer.listen(8080, function () {
    console.log('listening on http://localhost:8080');
});



//db implementation


const connectionString = 'postgresql://postgres:admin@localhost:5432/postgres';

// const pool = new Pool({
//     connectionString: connectionString,
// });

// pool.query('SELECT * from ds_project.foursquare', (err, res) => {
//     console.log(res.rows);
//     pool.end();
// });

const client = new Client({
    connectionString: connectionString,
});
client.connect();

app.route('/getFoursquare').get((req, res) => {
    client.query('SELECT * from ds_project.foursquare', (err_db, res_db) => {
        res.send({
            rows: res_db.rows
        });
    });
});

app.get("/saveFoursquare", (req, res) => {
    var insert = "insert into ds_project.foursquare (name, foursquare_id, img, open, likes, url, lat, lng) values (";
    insert+= "'" + req.query.name + "', ";
    insert+= "'" + req.query.foursquare_id + "', ";
    insert+= "'" + req.query.img + "', ";
    insert+= "'" + req.query.open + "', ";
    insert+= "" + req.query.likes + ", ";
    insert+= "'" + req.query.url + "', ";
    insert+= "" + req.query.lat + ", ";
    insert+= "" + req.query.lng + ")";
    console.log(insert);
    client.query(insert, (err_db, res_db) => {
        res.json({
            "db_response" : res_db
        });
    });
});



app.route('/getFacebook').get((req, res) => {
    client.query('SELECT * from ds_project.facebook', (err_db, res_db) => {
        res.send({
            rows: res_db.rows
        });
    });
});

app.get("/saveFacebook", (req, res) => {
    var insert = "insert into ds_project.facebook (facebook_id, name, description, checkins," +
        " engagement, isopen, link, img, url, lat, lng, overal_rating, price_range)" +
        "    values (";
    insert+= "'" + req.query.facebook_id + "', ";
    insert+= "'" + req.query.name + "', ";
    insert+= "'" + req.query.description + "', ";
    insert+= "" + req.query.checkins + ", ";
    insert+= "" + req.query.engagement + ", ";
    insert+= "'" + req.query.isopen + "', ";
    insert+= "'" + req.query.link + "', ";
    insert+= "'" + req.query.img + "', ";
    insert+= "'" + req.query.url + "', ";
    insert+= "" + req.query.lat + ", ";
    insert+= "" + req.query.lng + ", ";
    insert+= "" + req.query.overal_rating + ", ";
    insert+= "'" + req.query.price_range + "')";
    console.log(insert);
    client.query(insert, (err_db, res_db) => {
        res.json({
            "db_response" : res_db
        });
    });
});


app.route('/getTwitter').get((req, res) => {
    client.query('SELECT * from ds_project.twitter', (err_db, res_db) => {
        res.send({
            rows: res_db.rows
        });
    });
});

app.get("/saveTwitter", (req, res) => {
    var insert = "insert into ds_project.twitter (twitter_id, tweet, tweet_link, username," +
        " user_image, lat, lng)" +
        "    values (";
    insert+= "'" + req.query.twitter_id + "', ";
    insert+= "'" + req.query.tweet + "', ";
    insert+= "'" + req.query.tweet_link + "', ";
    insert+= "'" + req.query.username + "', ";
    insert+= "'" + req.query.user_image + "', ";
    insert+= "" + req.query.lat + ", ";
    insert+= "" + req.query.lng + ")";

    client.query(insert, (err_db, res_db) => {
        res.json({
            "db_response" : res_db
        });
    });
});

app.route('/getInstagram').get((req, res) => {
    client.query('SELECT * from ds_project.instagram', (err_db, res_db) => {
        res.send({
            rows: res_db.rows
        });
    });
});

app.get("/saveInstagram", (req, res) => {
    var insert = "insert into ds_project.instagram (instagram_id, name, description, img, url, lat, lng)" +
        "    values (";
    insert+= "'" + req.query.instagram_id + "', ";
    insert+= "'" + req.query.name + "', ";
    insert+= "'" + req.query.description + "', ";
    insert+= "'" + req.query.img + "', ";
    insert+= "'" + req.query.url + "', ";
    insert+= "" + req.query.lat + ", ";
    insert+= "" + req.query.lng + ")";

    client.query(insert, (err_db, res_db) => {
        res.json({
            "db_response" : res_db
        });
    });
});


