let ID = {
};

const randomPositions = [{"lat": 58.37792231245626, "lng": 26.72580726515949},
    {"lat": 58.3707, "lng": 26.727757},
    {"lat": 58.370022190708154, "lng": 26.72348214207258},
    {"lat": 58.37786997912684, "lng": 26.729887589208133},
    {"lat": 58.377647, "lng": 26.729156}];

let main = (function(){


    let userPosition = {};



    const distance_between_peers = 1;

    let init = function(){
        Geo.init();
    };

    let getDistanceFromLatLonInKm = function (lat1, lon1, lat2, lon2) {
        let R = 6371; // Radius of the earth in km
        let dLat = _deg2rad(lat2-lat1);  // deg2rad below
        let dLon = _deg2rad(lon2-lon1);
        let a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(_deg2rad(lat1)) * Math.cos(_deg2rad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        let d = R * c; // Distance in km
        return d;
    };

    let _deg2rad = function (deg) {
        return deg * (Math.PI/180)
    };

    return {
        init: init,
        getDistanceFromLatLonInKm: getDistanceFromLatLonInKm
    };

})();

$(document).ready(function(){
    main.init();
    //test distance method
    //F12(browser console) to see the results
    for (let i = 1; i < randomPositions.length; i++) {
        let distance = main.getDistanceFromLatLonInKm(randomPositions[0].lat, randomPositions[0].lng,
            randomPositions[i].lat, randomPositions[i].lng);
        console.log("Distance between the current peer(user) point " + 0 + ", and point " + i + " is :" + distance);
    }

    //simulate adding of new peers on the map where the 1'st point is the main peer(current user)
    for (let i = 0; i < randomPositions.length; i++) {
        Geo.addPins(randomPositions[i], i + "point", main.getDistanceFromLatLonInKm(randomPositions[0].lat, randomPositions[0].lng,
            randomPositions[i].lat, randomPositions[i].lng));
    }
});