let LoadTestSimulation = (function(){

    let config = {
        number_of_users: 0,
        limit: 10,
        timer: 10000
    };

    let init = function(){
        //simulate steady increase
        let load_test = setInterval(function () {
            console.log(config.number_of_users);
            config.number_of_users++;
            //or as new tabs !!!
            // let randomnumber = Math.floor((Math.random()*100)+1);
            // window.open("http://localhost:8080/?location=random","_blank",'PopUp',randomnumber,'scrollbars=1,menubar=0,resizable=1,width=850,height=500');

            // see what will happen when an expected number of users are
            // present(for example we expect from 10-20 user at max
            // in a time period of 1 minute)
            // as it can be seen, the mesh network work totally fine, responsiveness is great, communication between peer
            // is easy when approximately 20users log into the network for a period of time of 1 minute

            window.open("http://localhost:8080/?location=random","test_load" + config.number_of_users + "");
            if(config.number_of_users == config.limit) {
                clearInterval(load_test);
            }
        }, config.timer);
    };

    return{
        init: init
    }

})();