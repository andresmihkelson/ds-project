ID = {
    btnLoadTest: "#btnLoadTest",
    btnSoakTest: "#btnSoakTest",
    btnSpikeTest: "#btnSpikeTest",
}

$(document).ready(function(){
    $(ID.btnLoadTest).click(function () {
        LoadTestSimulation.init();
    });

    $(ID.btnSoakTest).click(function () {
        SoakTestSimulation.init();
    });

    $(ID.btnSpikeTest).click(function () {
        SpikeTestSimulation.init();
    });
});