let SoakTestSimulation = (function(){

    let config = {
        number_of_users: 0,
        limit: 30,
        timer: 2500
    };

    let init = function(){
        //simulate steady increase
        let load_test = setInterval(function () {
            console.log(config.number_of_users);
            config.number_of_users++;

            // see what happens when a steady number of users is constanly added to the network increasing
            // limit should be higher compared to the limit when load testing is performed
            // here we expect around 30 to 50 users for a 1 minute time
            // which is tripple the size of the expected users per minute
            // and it can be seen that usually after 15users the initialization time
            // of the mesh network starts to slow down

            window.open("http://localhost:8080/?location=random","test_soak" + config.number_of_users + "");
            if(config.number_of_users == config.limit) {
                clearInterval(load_test);
            }
        }, config.timer);
    };

    return{
        init: init
    }

})();