let SpikeTestSimulation = (function(){

    let config = {
        number_of_users: 0,
        limit: 2,
        timer: 5000,
        batch_size_users: 10
    };

    let init = function(){
        //simulate steady increase
        let load_test = setInterval(function () {
            console.log(config.number_of_users);
            config.number_of_users++;

            // see what happens if suddenly many users come in the network at the same time
            // here we have on every timer value(of around 5sec, a big number of users is suddenly coming to the network)
            // it can be seen that our network is not really good when a huge number of
            // the response time is huge, and the application itself becomes slower when compared with the expected
            // number of users to be present per given minute

            for(let i=0; i < config.batch_size_users; i++) {
                window.open("http://localhost:8080/?location=random","test_spike" + config.number_of_users + "" + i);
            }

            if(config.number_of_users >= config.limit) {
                clearInterval(load_test);
            }
        }, config.timer);
    };

    return{
        init: init
    }

})();