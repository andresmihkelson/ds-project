let ID = {
    type: "#type",
    data_container: "#data_container"
};

let crawlerMain = (function(){

    let component = null;
    let type;

    let init = function(){
        type = _getParameterByName("type");
        $(ID.type).text(type);
        if(type !== "client"){
            component = CrawlerComponent;
            component.init(type);
        }
    };

    let _getParameterByName = function(name, url){
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };

    return {
        init: init
    };

})();

$(document).ready(function(){
    crawlerMain.init();
});