let Geo = (function () {
    let osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    let osmAttribution = 'Map data &copy; <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
    let osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 18, attribution: osmAttribution});
    let map;
    let lat = 58.38;
    let lon = 26.76;
    let lat_lng = [];
    let markers = {};
    let enableHighAccuracy = false;
    let init = function () {
        map = L.map('map');
        map.setView([lat, lon], 13);
        map.addLayer(osm);

        if ("geolocation" in navigator) {
            enableHighAccuracy = true
        }
    };

    let addPins = function (pos, guest = '', distance = 0) {
        lat_lng.push(pos);
        if(enableHighAccuracy) {
            let myLocationPin;
            // Put a pin on the map
            if(lat_lng.length > 1) {
                myLocationPin = L.marker([pos.lat, pos.lng])
                    .bindPopup("<div class='map_popup'>Guest_" + guest.substr(0, 3)
                        + "<br/>"
                        + "Distance: " + distance.toFixed(2) + " km</div>");
                myLocationPin.addTo(map);
                markers[guest] = myLocationPin;
            }
            //always the user last
            myLocationPin = L.marker([lat_lng[0].lat, lat_lng[0].lng], {icon: _userIcon()})
                .bindPopup("<div class='map_popup'>It is me!</div>");
            myLocationPin.addTo(map);

            // Move map viewport to the pin
            if(lat_lng.length < 2) {
                map.setView([pos.lat, pos.lng], 14);
            }
        } else {
            console.log("High Accuracy is not supported in your broswer!");
        }
    };

    let removePin = function (guest) {
        if(markers.hasOwnProperty(guest)) {
            map.removeLayer(markers[guest]);
            delete markers[guest];
        };
    };

    let _userIcon = function () {
        return new L.Icon({
            iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
            shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
        });
    };

    return {
        init: init,
        addPins: addPins,
        removePin: removePin
    }
})();