let ClientComponent = (function(){

    let onReceiveData = null;

    let userPosition = {};

    let init = function(userPos, onRecData, onClosed){
        userPosition = userPos;
        onReceiveData = onRecData;
        EasyRTCComponent.connect(
            function(who, msgType, content){
                if(onReceiveData){
                    onReceiveData(who, msgType, content);
                }
                },
            function(){_findPeerAndConnect();},
            function(who){if(onClosed){onClosed(who);}}
        );
    };


    let findPeerIndex = function(who){
        return EasyRTCComponent.getIndexInOccupantsList(who);
    };

    let _findPeerAndConnect = function(){
        let current_date = new Date();
        let peers = EasyRTCComponent.getPeers();
        for(let peer in peers){
            EasyRTCComponent.connectToPeer(peer, () => {
                EasyRTCComponent.sendDataP2P(peer, "msg",
                    {"lat": userPosition.lat,
                    "lng": userPosition.lng,
                    "distance_m": 500,
                    "count" : 100,
                    "twitter_type" : 'recent',
                    "version" : 20181005,
                    "distance_km" : 1,
                    "current_time": current_date.getTime()});
            });
        }
    };

    let likeEvent = function(event_id, event_name,  event_type, lat, lng, minus){
        let peers = EasyRTCComponent.getPeers();

        let data = {
            "event_id": event_id,
            "event_name": event_name,
            "event_type": event_type,
            "lat": lat,
            "lng": lng
        };

        if(minus){
            data.minus = 1;
        }

        for(let peer in peers){
            EasyRTCComponent.sendDataP2P(peer, "like", data);
        }
    };

    let updateStatus = function(text, lat, lng){
        text = "<b>Guest_[</b> near you: " + text;
        let peers = EasyRTCComponent.getPeers();

        for (let peer in peers){
            EasyRTCComponent.sendDataP2P(peer, "shared_status",{
                "text": text,
                "lat": lat,
                "lng": lng
            });
        }
    };


    let sendLocation = function(data, who){
        let current_date = new Date();
        data.current_time = current_date.getTime();
        EasyRTCComponent.sendDataP2P(who, "msg2", data);
    };

    return {
        init: init,
        likeEvent: likeEvent,
        updateStatus: updateStatus,
        findPeerIndex: findPeerIndex,
        sendLocation: sendLocation
    };
})();