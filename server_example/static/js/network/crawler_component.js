let CrawlerComponent = (function(){

    let type;

    let distance = 1;

    let init = function(crawler_type){
        type = crawler_type;
        EasyRTCComponent.connect(function(who, msgType, content){_onReceiveData(who, msgType, content);});
    };

    let _onReceiveData = function(who, msgType, content){
        if(msgType !== "msg") return;

        if (type === 'facebook') {
            window.FacebookCrawler.getPlacesNearLocation(content.lat, content.lng, content.distance_m, function(data){
                EasyRTCComponent.sendDataP2P(who, type, _filterByLocation(content.lat, content.lng, data));
            });
        } else if (type === 'twitter') {
            window.TwitterCrawler.getTweetsByLocation(content.count, content.lat, content.lng, content.distance_km, content.twitter_type, function(data){
                EasyRTCComponent.sendDataP2P(who, type, _filterByLocation(content.lat, content.lng, data))
            });
        } else if (type === 'instagram') {
            window.InstagramCrawler.getPlacesNearLocation(content.lat, content.lng, content.distance_m, function(data){
                EasyRTCComponent.sendDataP2P(who, type, _filterByLocation(content.lat, content.lng, data))
            });
        } else if (type === 'foursquare') {
            window.FoursquareCrawler.searchVenues(content.lat, content.lng, content.version, function(data){
                EasyRTCComponent.sendDataP2P(who, type, _filterByLocation(content.lat, content.lng, data))
            });
        }


    };

    let _filterByLocation = function (lat1, lng1, data) {
        let filtered_data = [];
        for (let i = 0; i < data.length; i++) {
            if(_getDistanceFromLatLonInKm(lat1, lng1, data[i].lat, data[i].lng) <= distance) {
                filtered_data.push(data[i]);
            }
        }
        return filtered_data;
    };

    let _getDistanceFromLatLonInKm = function (lat1, lon1, lat2, lon2) {
        let R = 6371; // Radius of the earth in km
        let dLat = _deg2rad(lat2-lat1);  // deg2rad below
        let dLon = _deg2rad(lon2-lon1);
        let a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(_deg2rad(lat1)) * Math.cos(_deg2rad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        let d = R * c; // Distance in km
        return d;
    };

    let _deg2rad = function (deg) {
        return deg * (Math.PI/180)
    };

    return {
        init: init
    };
})();