let EasyRTCComponent = (function(){
    let selfEasyrtcid = "";
    let connectList = {};
    let channelIsActive = {};
    /**
     *  callback function for what should happen after receiving data
     */
    let onReceiveData = null;

    /**
     *  callback function for what should happen after connecting to peer
     */
    let callback = null;

    let hasConnectedOnce = false;

    let on_closed_listener = null;

    let connect = function(onrd, cb, on_closed){
        easyrtc.enableDebug(false);
        easyrtc.enableDataChannels(true);
        easyrtc.enableVideo(false);
        easyrtc.enableAudio(false);
        easyrtc.enableVideoReceive(false);
        easyrtc.enableAudioReceive(false);
        easyrtc.setDataChannelOpenListener(openListener);
        easyrtc.setDataChannelCloseListener(closeListener);
        easyrtc.setPeerListener(peerListener);
        easyrtc.setRoomOccupantListener(roomOccupantListener);
        easyrtc.connect("easyrtc.dataMessaging", loginSuccess, loginFailure);
        onReceiveData = onrd ? onrd : null;
        callback = cb ? cb : null;
        on_closed_listener = on_closed ? on_closed : null;
    };

    let openListener = function(otherParty){
        channelIsActive[otherParty] = true;
    };

    let closeListener = function(otherParty){
        channelIsActive[otherParty] = false;
        if(on_closed_listener) {
            on_closed_listener(otherParty);
        }
    };

    let peerListener = function(who, msgType, content) {
        if(onReceiveData){
            onReceiveData(who, msgType, content);
        }
    };

    let roomOccupantListener = function(roomName, occupantList, isPrimary){
        connectList = occupantList;
        if(!hasConnectedOnce && callback){
            hasConnectedOnce = true;
            callback();
        }
    };

    let getPeers = function(){
        return connectList;
    };

    let connectToPeer = function(otherEasyrtcid, callback){
        if (easyrtc.getConnectStatus(otherEasyrtcid) === easyrtc.NOT_CONNECTED) {
            try {
                easyrtc.call(otherEasyrtcid, function(caller, media) { // success callback
                    if (media === 'datachannel') {
                        connectList[otherEasyrtcid] = true;
                        if(callback) callback();
                    }
                }, function(errorCode, errorText) {
                    connectList[otherEasyrtcid] = false;
                    easyrtc.showError(errorCode, errorText);
                }, function(wasAccepted) {
                    console.info(wasAccepted);
                });
            }catch( callerror) {
                console.log("saw call error ", callerror);
            }
        }
        else {
            //easyrtc.showError("ALREADY-CONNECTED", "already connected to " + easyrtc.idToName(otherEasyrtcid));
        }
    };

    let sendDataP2P = function(otherEasyrtcid, messageType, data){
        if (easyrtc.getConnectStatus(otherEasyrtcid) === easyrtc.IS_CONNECTED) {
            easyrtc.sendDataP2P(otherEasyrtcid, messageType, data);
        }
        else {
            connectToPeer(otherEasyrtcid, () => {
                easyrtc.sendDataP2P(otherEasyrtcid, messageType, data);
            });
        }
    };

    let loginSuccess = function(easyrtcid){
        selfEasyrtcid = easyrtcid;
    };

    let loginFailure = function(errorCode, message) {
        easyrtc.showError(errorCode, "failure to login");
    };

    let getIndexInOccupantsList = function(who){
        let index = 0;
        for (let key in connectList) {
            if(key === who){
                break;
            }
            index++;
        }
        return index;
    };

    return {
        connect: connect,
        sendDataP2P: sendDataP2P,
        connectToPeer: connectToPeer,
        getPeers: getPeers,
        getIndexInOccupantsList: getIndexInOccupantsList
    };

})();