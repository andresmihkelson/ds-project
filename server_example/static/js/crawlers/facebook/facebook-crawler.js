let FacebookCrawler = (function(){

    let access_token_app = "270346136935896|e0d04f62e1f4925b2321ed30aeffb753";
    let base_facebook_url = "https://graph.facebook.com/v2.7/";

    let getPlacesNearLocation = function(lat, lon, dist, callback){
        $.ajax({
            type: "GET",
            url: base_facebook_url + "search?type=place&fields=about,description,hours,is_always_open,category_list,cover,engagement,link,overall_star_rating,rating_count,website,phone,price_range,location,name,checkins,picture&center=" + lat + "," + lon + "&distance=" + dist + "&access_token=" + access_token_app,
            success: function(data) {
                let fb_array = [];
                for(let i=0;i<data.data.length;i++){
                   let curr_obj = data.data[i];
                    let fb_obj = {
                        "facebook_id" : curr_obj.id,
                        "name": curr_obj.name,
                        "description": curr_obj.description,
                        "checkins" : curr_obj.checkins,
                        "engagement" : curr_obj.engagement.count,
                        "isopen" : curr_obj.is_always_open,
                        "link" : curr_obj.link,
                        "img" : curr_obj.picture.data.url,
                        "url": curr_obj.website,
                        "lat" : curr_obj.location.latitude,
                        "lng" : curr_obj.location.longitude,
                        "overal_rating" : curr_obj.overall_star_rating,
                        "price_range" : curr_obj.price_range
                    };
                    fb_array.push(fb_obj);
                    $.ajax({
                        type: "get",
                        url: '/saveFacebook',
                        data:  fb_obj
                    });
                }
                callback(fb_array);
            }, error(data) {
                $.ajax({
                    async: false,
                    type: "get",
                    url: '/getFacebook',
                    success: function (data) {
                        console.log(data);
                        fb_array = data.rows;
                        callback(fb_array);
                    }
                });
            }
        });
    };

    return {
        getPlacesNearLocation: getPlacesNearLocation
    };
})();

window.FacebookCrawler = FacebookCrawler;




