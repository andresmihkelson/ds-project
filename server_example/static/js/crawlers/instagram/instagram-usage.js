let ID = {
    container: "#container"
};

$(document).ready(function(){
    window.InstagramCrawler.getPlacesNearLocation(58.3780,26.7290,500, function(data){
        console.log(data);
        let html = "";
        for(let i=0;i<data.data.length;i++){
            html += data.data[i].name;
            html += "<br/>";
        }
        $(ID.container).html(html);

        window.InstagramCrawler.getAdditionalInfoAboutLocations(data.data);
    });
});