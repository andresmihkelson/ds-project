let InstagramCrawler = (function(){

    let access_token_app = "8650472545.297a9bc.5c3fcd1ef4384ea49845486c85797a7e";
    let base_instagram_url = "https://api.instagram.com/v1/";

    let getPlacesNearLocation = function(lat, lon, dist, callback){
        $.ajax({
            async: false,
            type: "get",
            url: '/getInstagram',
            success: function (data) {
                let instagram_array = data.rows;
                callback(instagram_array);
            }
        });
        // $.ajax({
        //     type: "GET",
        //     url: base_instagram_url + "locations/search?"+
        //         "lat=" + lat  +
        //         "&lng=" + lon +
        //         "&distance=" + dist +
        //         "&access_token=" + access_token_app,
        //     success: function(data) {
        //         let instagram_array = [];
        //         for(let i=0;i<data.data.length;i++){
        //             let instagram_object = {
        //                 instagram_id:   data.data[i].id,
        //                 name: data.data[i].name,
        //                 description: "",
        //                 img: "",
        //                 url: "",
        //                 lat: data.data[i].latitude,
        //                 lng: data.data[i].longitude
        //             };
        //
        //             if(i < 5){
        //                 getDetailsForLocation(data.data[i].id, function(data){
        //                     if(data[0]){
        //                         instagram_object.description = data[0].caption.text;
        //                         instagram_object.url = data[0].link;
        //                         if(data[0].type === "image"){
        //                             instagram_object.img = data[0].images.low_resolution.url;
        //                         }
        //                     }
        //                 });
        //             }
        //
        //             instagram_array.push(instagram_object);
        //
        //             $.ajax({
        //                 type: "get",
        //                 url: '/saveInstagram',
        //                 data:  instagram_object
        //             });
        //         }
        //         callback(instagram_array);
        //     }, error: function(){
        //         $.ajax({
        //             async: false,
        //             type: "get",
        //             url: '/getInstagram',
        //             success: function (data) {
        //                 let instagram_array = data.rows;
        //                 callback(instagram_array);
        //             }
        //         });
        //     }
        // });
    };

    let getDetailsForLocation = function(locationId, callback){
        $.ajax({
            type: "GET",
            async: false,
            url: base_instagram_url + "locations/" + locationId +
                "/media/recent" +
                "?access_token=" + access_token_app,
            success: function(data){
                callback(data);
            }, error: function(data){
                callback(data);
            }
        });
    };

    let getPlacesByHashtag = function(hashtag, callback){
        $.ajax({
            type: "GET",
            url: base_instagram_url + "tags/search?q="+ hashtag +
                "&access_token=" + access_token_app,
            success: function(data) {
                callback(data);
            }
        });
    };

    let getMediaForHashtag = function(hashtag, callback){
        $.ajax({
            type: "GET",
            url: base_instagram_url + "tags/"+ hashtag + "/media/recent" +
            "?access_token=" + access_token_app,
            success: function(data) {
                callback(data);
            }
        });
    };

    let getAdditionalInfoAboutLocations = function(locations){
      for(let i=0;i<locations.length;i++){
          $.ajax({
              type: "GET",
              url: base_instagram_url + "locations/" + locations[i].id +
                  "/media/recent" +
                  "?access_token=" + access_token_app,
              success: function(data) {
                  console.log(data);
              }
          });
      }
    };



    return {
        getPlacesNearLocation: getPlacesNearLocation,
        getPlacesByHashtag: getPlacesByHashtag,
        getAdditionalInfoAboutLocations: getAdditionalInfoAboutLocations,
        getMediaForHashtag: getMediaForHashtag
    };
})();

window.InstagramCrawler = InstagramCrawler;




