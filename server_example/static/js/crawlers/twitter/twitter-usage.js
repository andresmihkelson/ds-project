let ID = {
    container: "#container"
};

$(document).ready(function(){
    window.TwitterCrawler.getStatuses(100, 'twitterapi', function(data){
        //console.log(data);
        //let html = "";
        // for(let i=0;i<data.length;i++){
        //     html += data[i].text;
        //     html += "<br/>";
        // }
        // $(ID.container).html(html);
    });

    window.TwitterCrawler.getTweetsByLocation(100, 58.3780, 26.7290, 1, 'recent', function(data){
        console.log(data);
        let html = "";
        for(let i=0;i<data.statuses.length;i++){
            text = data.statuses[i].text;
            let k = text.indexOf("https:");
            tweet = text.substr(0, k);
            tweetLink = text.substr(k, text.length -1);
            html += tweet;
            html += "<div><span> user:" + data.statuses[i].user.name + "</span></div>";
            html += "<div>" + "<img src='" + data.statuses[i].user.profile_image_url +"'/>" + "</div>"
            html += "<span>Full tweet on: <a href='" + tweetLink + "'>" + tweetLink + "</a></span>";
            html += "<br/><br/>";
        }
        $(ID.container).html(html);
    });
});