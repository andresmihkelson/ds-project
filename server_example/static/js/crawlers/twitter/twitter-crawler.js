let TwitterCrawler = (function(){

    let consumer_key = 'gbeAbHPsBhTZV0ZZyrywO1Jzm';
    let consumer_secret = '0yE8FpYa6iiJWKAlQaYnojnm2OTd8GqmeU9cUE37wrX4AqTbix';
    let bearer_token = 'Bearer AAAAAAAAAAAAAAAAAAAAAArLugAAAAAAHvBTyALiLoI8bKbCMRhsvMso2Zs%3DUbzmFU13gqg0aXyfOLbdw07O1y1nxjLfffOv11CowkCBEBZUVn';

    let base_twitter_url = "https://api.twitter.com/1.1/";

    let getStatuses = function(count, screen_name, callback) {
        $.ajax({
            type: "GET",
            headers: {"Authorization": bearer_token},
            url: base_twitter_url + "statuses/user_timeline.json?count=" + count + "&screen_name=" + screen_name,
            success: function (data) {
                callback(data);
            }
        });
    };

    let getTweetsByLocation = function(count, lat, lon, radius, tweet_type, callback) {
        $.ajax({
            type: "GET",
            headers: {"Authorization": bearer_token},
            url: base_twitter_url + "search/tweets.json?q=tartu,&result_type=" + tweet_type + "&count=" + count + "&geocode=" + lat + "," + lon + "," + radius + "km",
            success: function (data) {
                let twitter_array = [];
                for(let i=0;i<data.statuses.length;i++){
                    let text = data.statuses[i].text;
                    let k = text.indexOf("https:");
                    let tweet = text.substr(0, k);
                    let tweetLink = text.substr(k, text.length -1);

                    if(!tweet){
                        continue;
                    }

                    let twitter_object = {
                        twitter_id:   data.statuses[i].id,
                        tweet: tweet,
                        tweet_link: tweetLink,
                        username: data.statuses[i].user.name,
                        user_image: data.statuses[i].user.profile_image_url,
                        lat: data.statuses[i].geo ? data.statuses[i].geo.coordinates[0] : "",
                        lng: data.statuses[i].geo ? data.statuses[i].geo.coordinates[1] : ""
                    };
                    twitter_array.push(twitter_object);

                    $.ajax({
                        type: "get",
                        url: '/saveTwitter',
                        data:  twitter_object
                    });
                }
                callback(twitter_array);
            }, error() {
                $.ajax({
                    async: false,
                    type: "get",
                    url: '/getTwitter',
                    success: function (data) {
                        let twitter_array = data.rows;
                        callback(twitter_array);
                    }
                });
        }
        });
    };
    return {
        getStatuses: getStatuses,
        getTweetsByLocation: getTweetsByLocation
    };
})();

window.TwitterCrawler = TwitterCrawler;




