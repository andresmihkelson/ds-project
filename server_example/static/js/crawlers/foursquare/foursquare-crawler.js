let FoursquareCrawler = (function(){

    // if quota exceeded uncomment the following two lines and comment the next two lines or vice versa
    //
    let client_id = "UPPWAYAKJ4WRNNLU5YRVK4HLOZ1VXSSQRFB435TZQMYMU2N1";
    let client_secret = "24YPT1XR5JHXRBGFWZ4OOJG0OPPH1NZ2SC03XPBTAK1Q1KXY";

    let base_foursquare_url = "https://api.foursquare.com/v2/venues/";

    let searchVenues = function(lat, lon, v, callback){
        //test
        console.log(v);
        $.ajax({
            type: "GET",
            url: base_foursquare_url + "search?" + "ll=" + lat + "," + lon + "&client_id=" + client_id + "&client_secret=" + client_secret + "&v=" + v,
            success: function (data) {
                let venue_array = [];
                for (let i = 0; i < data.response.venues.length; i++) {
                    if(i > 4) {
                        break;
                    }
                    let name = data.response.venues[i].name;
                    let row = data.response.venues[i];
                    venue(row.id, v, function (single_data) {
                        if (single_data.status) {
                             $.ajax({
                                 async: false,
                                 type: "get",
                                 url: '/getFoursquare',
                                 success: function (data) {
                                     venue_array = data.rows;
                                     console.log(venue_array);
                                     i=5;
                                 }
                             });
                        } else {
                            console.log(single_data);
                            console.log(row.id);
                            let obj = single_data.response.venue;
                            let venue_obj = {
                                "name": name,
                                "foursquare_id": row.id,
                                "img": obj.bestPhoto ? (obj.bestPhoto.prefix + "300x300" + obj.bestPhoto.suffix) : "",
                                "open": obj.hours ? obj.hours.isOpen : "",
                                "likes": obj.likes ? obj.likes.count : "",
                                "url": obj.shortUrl,
                                "lat": obj.location.lat,
                                "lng": obj.location.lng
                            };
                            $.ajax({
                                type: "get",
                                url: '/saveFoursquare',
                                data:  venue_obj
                            });
                            venue_array.push(venue_obj);
                        }
                    });
                }
                callback(venue_array);
            }
        });
    };

    let exploreVenues = function(lat, lon, v, callback){
        $.ajax({
            type: "GET",
            url: base_foursquare_url + "explore?" + "ll=" + lat + "," + lon + "&client_id=" + client_id + "&client_secret=" + client_secret + "&v=" + v,
            success: function(data) {
                callback(data);
            }
        });
    };

    let trendingVenues = function(lat, lon, v, callback){
        $.ajax({
            type: "GET",
            url: base_foursquare_url + "trending?" + "ll=" + lat + "," + lon + "&client_id=" + client_id + "&client_secret=" + client_secret + "&v=" + v,
            success: function(data) {
                callback(data);
            }
        });
    };

    let venue = function (id, v, callback) {
        $.ajax({
            async: false,
            type: "GET",
            url: base_foursquare_url + id + "?client_id=" + client_id + "&client_secret=" + client_secret + "&v=" + v,
            success: function(data) {
                callback(data);
            }, error: function (data) {
                callback(data);
            }
        });
    }

    return {
        searchVenues: searchVenues,
        exploreVenues: exploreVenues,
        trendingVenues: trendingVenues,
        venue: venue
    };
})();

window.FoursquareCrawler = FoursquareCrawler;




