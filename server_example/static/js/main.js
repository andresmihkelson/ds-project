let ID = {
    facebook: "#facebook",
    facebook_body: "#facebook-body",
    instagram: "#instagram",
    instagram_body: "#instagram-body",
    twitter: "#twitter",
    twitter_body: "#twitter-body",
    foursquare: "#foursquare",
    foursquare_body: "#foursquare-body",
    like: "#like",
    share_status: "#share_status",
    update_status_btn: "#update_status_btn",
    status: "#status",
    updates: "#updates",
    heart: "#heart",
    show_map: "#show_map",
    container_map: "#container_map"
};

let startTime;

let main = (function(){

    let counter = 0;
    let component = null;
    let userLikes = [];
    let allLikes = [];

    let userPosition = {};

    // 9 - where we at
    // 0, 3 - close to us
    // 7, 10 - far away from us
    const randomPositions = [{"lat": 58.37792231245626, "lng": 26.72580726515949}, //0 close to cs inst
        {"lat": 58.3707, "lng": 26.727757}, // 1 (more than 1km than cs inst)
        {"lat": 58.370022190708154, "lng": 26.72348214207258}, // 2
        {"lat": 58.37786997912684, "lng": 26.729887589208133}, // 3 close to cs inst
        {"lat": 58.377647, "lng": 26.719156}, // 4 close to cs inst
        {"lat": 58.378157, "lng": 26.729156}, // 5 close to cs isnt
        {"lat": 58.379267, "lng": 26.739156}, // 6 (more than 1km than cs inst)
        {"lat": 58.375717, "lng": 26.789156}, // 7 no data for every crawler (far away)
        {"lat": 58.37783040000001, "lng": 26.735411199999998}, //8 h
        {"lat": 58.3783, "lng": 26.7151}, // 9 tartu cs institute
        {"lat": 58.377717, "lng": 26.778836} // 10 far away
    ];

    const distance_between_peers = 1;

    let init = function(){
        Geo.init();
        $(ID.container_map).hide();
        let get_param = _getParameterByName('location');
        let id = _getParameterByName('id');
        if (navigator.geolocation && get_param !== 'random') {
            navigator.geolocation.getCurrentPosition(_postInit, _mapError);
        } else {
            _mapError(id);
        }
    };

    let _postInit = function(position){
        userPosition = position.lat ? position : {"lat": position.coords.latitude, "lng": position.coords.longitude};

        console.info(userPosition);
        component = ClientComponent;
        component.init(userPosition, _onReceiveData, _onClosedListener);
        Geo.addPins(userPosition);
        _initComponents();
    };

    let _onClosedListener = function(who){
        Geo.removePin(who);
    };

    let _mapError = function(id){
        let idx = !id? Math.floor(Math.random() * Math.floor(randomPositions.length)) : id;
        _postInit(randomPositions[idx]);
    };

    let _initComponents = function(){
        $(ID.update_status_btn).click(function(){
           let text = $(ID.status).val();
           if(text.trim() !== ""){
               component.updateStatus(text, userPosition.lat, userPosition.lng);
               _onReceiveStatusUpdate({"text": "You: " + text});
           }
           $(ID.status).val("");
        });

        $('body').on('click', '.event-title i', function() {
            let data = $(this).data();
            let event_id = data.eventId;
            let event_name = data.eventName;
            let event_type = data.eventType;

            let minus = false;
            if(userLikes.indexOf(event_id) > -1){
                $(this).removeClass("fas");
                $(this).addClass("far");
                userLikes.splice(userLikes.indexOf(event_id), 1);
                _updateAllLikes({"event_id": event_id, "event_type": event_type, "event_name": event_name, "minus": 1});
                minus = true;
            } else {
                $(this).removeClass("far");
                $(this).addClass("fas");
                userLikes.push(event_id);
                _onReceiveStatusUpdate({text: "You have just liked " + event_name + " on " + event_type});
                _updateAllLikes({"event_id": event_id, "event_type": event_type, "event_name": event_name});
            }

            component.likeEvent(event_id, event_name, event_type, userPosition.lat, userPosition.lng, minus);

        });


        $(ID.show_map).click(function () {
            $(ID.container_map).animate({
                height: "toggle"
            }, 1000, function() {
                // Animation complete.
            });
        });

        window.addEventListener("beforeunload", function (e) {
            return true;
        });
    };


    let _onReceiveData = function(who, msgType, data){
        if(msgType === "facebook"){
            _onReceiveFacebook(data);
            counter++;
        } else if (msgType === "foursquare"){
            _onReceiveFoursquare(data);
            counter++;
        } else if (msgType === "twitter"){
            _onReceiveTwitter(data);
            counter++;
        } else if (msgType === "instagram"){
            _onReceiveInstagram(data);
            counter++;
        } else if (msgType === "like"){
            _onReceiveLike(data, who);
        } else if (msgType === "shared_status"){
            let idPeer = component.findPeerIndex(who);
            data.text = data.text.replace("[", who.substr(0, 3));
            _onReceiveStatusUpdate(data);
        } else if (msgType === "msg"){
            let d = new Date();
            console.log("Connection time ", d.getTime() - data.current_time);
            component.sendLocation({"lat": userPosition.lat, "lng": userPosition.lng}, who);
            Geo.addPins(data, who,
                _getDistanceFromLatLonInKm(data.lat, data.lng, userPosition.lat, userPosition.lng));
            console.info("received data from: " + who);
        } else if(msgType === "msg2"){
            let d = new Date();
            console.log("Connection time ", d.getTime() - data.current_time);
            Geo.addPins(data, who,
                _getDistanceFromLatLonInKm(data.lat, data.lng, userPosition.lat, userPosition.lng));
            console.info("received data from: " + who);
        } else if(msgType === "leave_room"){
            //TODO: remove pin from map
            console.info(who + " left the room");
        }

        if(counter == 4 && (msgType === 'facebook' || msgType === 'twitter'
            || msgType === 'instagram' || msgType === 'foursquare')) {
            let d = new Date();
            console.log("Init time", d.getTime() - startTime.getTime());
        }
    };

    let _onReceiveFacebook = function(data){
        let html = '';

        for(let i=0;i<data.length;i++){
            let description = data[i].description;
            description = description && description.length > 100 ? description.substr(0, 100) + "..." : description;
            if(!description){
                continue;
            }
            html += `<div class="facebook-header-${i%2}">`;
            html += `<div class="event-title" id="${data[i].facebook_id}">`;
            html += `<h4>${data[i].name}</h4>`;
            html += `<i id="heart" data-event-id="${data[i].facebook_id}" data-event-name="${data[i].name}" data-event-type="facebook" class="far fa-heart"></i>`; //<i class="fas fa-heart"></i>
            html += `<small>(<span id="numberLikes" data-event-id-span="${data[i].facebook_id}">0</span> have liked this.)</small>`;
            html += `</div>`;
            html += `<div class="facebook-body">`;
            html += `<img src="${data[i].img}" height="50"/>`;
            html += `<div>`;
            html += `<small>${description}</small><br/>`;
            if(data[i].overal_rating){
                html += `<small>Rating: <b>${data[i].overal_rating}/5</b></small><br/>`;
            }
                html += `<small>Is open: <b>${data[i].isopen ? "yes": "no"}</b></small><br/>`;
            html += `<small>Number of check-ins: <b>${data[i].checkins}</b></small><br/>`;
            html += `<small><a href="${data[i].link}">See more on Facebook</a></small>`;
            html += `</div>`;
            html += `</div>`;
            html += `</div>`;
        }

        $(ID.facebook_body).html(html);
    };

    let _onReceiveInstagram = function(data){
        let html = '';

        for(let i=0;i<data.length;i++){
            let row = data[i];
            let description = row.description;
            description = description && description.length > 100 ? description.substr(0, 100) + "..." : description;

            html += `<div class="instagram-header-${i%2}">`;
            html += `<div class="event-title" id="${data[i].instagram_id}">`;
            html += `<h4>${data[i].name}</h4>`;
            html += `<i id="heart" data-event-id="${data[i].instagram_id}" data-event-name="${data[i].name}" data-event-type="instagram" class="far fa-heart"></i>`; //<i class="fas fa-heart"></i>
            html += `<small>(<span id="numberLikes" data-event-id-span="${data[i].instagram_id}">0</span> have liked this.)</small>`;
            html += `</div>`;
            html += `<div class="instagram-body">`;
            html += `<img src="${row.img}" height="50"/>`;
            html += `<div>`;
            html += `<small>Description: <b>${description}</b></small><br/>`;
            html += `<small><a href="${row.url}">See more on Instagram</a></small>`;
            html += `</div>`;
            html += `</div>`;
            html += `</div>`;
        }

        $(ID.instagram_body).html(html);
    };

    let _onReceiveTwitter = function(data){
        let html = "";
        for(let i=0;i<data.length;i++){
            html += `<div class="twitter-content twitter-content-${i%2}">`;
            html += "<img src='" + data[i].user_image +"' height='50'/>";
            html += "<div>";
            html += `<div class="event-title" id="${data[i].facebook_id}">`;
            html += `<small class="twitter-username"><b>${data[i].username}</b></small><br/>`;
            html += `<i id="heart" data-event-id="${data[i].twitter_id}" data-event-name="${data[i].username}" data-event-type="twitter" class="far fa-heart"></i>`; //<i class="fas fa-heart"></i>
            html += `<small>(<span id="numberLikes" data-event-id-span="${data[i].twitter_id}">0</span> have liked this.)</small>`;
            html += `</div>`;
            html += `<small>${data[i].tweet}</small><br/>`;
            html += `<small><a href="${data[i].tweet_link}">See more</a></small>`;
            html += "</div>";
            html += `</div>`;
        }
        $(ID.twitter_body).html(html);
    };

    let _onReceiveFoursquare = function(data){
        let html = "";
        for(let i=0;i<data.length;i++){
            let row = data[i];
            html += `<div class="foursquare-header-${i%2}">`;
            html += `<div class="event-title" id="${data[i].foursquare_id}">`;
            html += `<h4>${data[i].name}</h4>`;
            html += `<i id="heart" data-event-id="${data[i].foursquare_id}" data-event-name="${data[i].name}" data-event-type="foursquare" class="far fa-heart"></i>`; //<i class="fas fa-heart"></i>
            html += `<small>(<span id="numberLikes" data-event-id-span="${data[i].foursquare_id}">0</span> have liked this.)</small>`;
            html += `</div>`;
            html += `<div class="foursquare-body">`;
            html += `<img src="${row.img}" height="50"/>`;
            html += `<div>`;
            if(row.open) {
                html += `<small>Is open: <b>${row.open == 'true' ? "yes" : "no"}</b></small><br/>`;
            } else {
                html += `<small>Is open: <b>No info</b></small><br/>`;
            }
            html += `<small>Number of likes: <b>${row.likes}</b></small><br/>`;
            html += `<small><a href="${row.url}">See more on Foursquare</a></small>`;
            html += `</div>`;
            html += `</div>`;
            html += `</div>`;
        }
        $(ID.foursquare_body).html(html);
    };

    let _updateAllLikes = function(data){
        let event_id = data.event_id;
        let found = allLikes.filter(
            function(data){ return data.event_id === event_id;}
        );
        let count = 0;
        if(found.length > 0){
            if(data.minus){
                found[0].count = Math.max(found[0].count - 1, 0);
            } else {
                found[0].count = found[0].count + 1;
            }
            count = found[0].count;
        } else {
            count = 1;
            if(data.minus){
                count = 0;
            }
            allLikes.push({"event_id" : event_id, "count": count});
        }

        data.count = count;
        _updateLikesInHtml(data);
    };

    let _updateLikesInHtml = function(data){
        let span = $("[data-event-id-span=" + data.event_id + "]");
        $(span).html(data.count);
    };

    let _onReceiveLike = function(data, who){
        if(!data.lat || _getDistanceFromLatLonInKm(userPosition.lat, userPosition.lng, data.lat, data.lng) <= distance_between_peers) {
            let text = "<b>Guest_" + who.substr(0,3) + "</b> has just liked " + data.event_name + " on " + data.event_type;
            _updateAllLikes(data);
            if (!data.minus) {
                _onReceiveStatusUpdate({"text": text});
            }
        }
    };

    let _onReceiveStatusUpdate = function(data){
        if(!data.lat || _getDistanceFromLatLonInKm(userPosition.lat, userPosition.lng, data.lat, data.lng) <= distance_between_peers) {
            let html = data.text;
            html += "<br/>";
            html += $(ID.updates).html();
            $(ID.updates).html(html);
        }
    };

    let _getParameterByName = function(name, url){
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };

    let _getDistanceFromLatLonInKm = function (lat1, lon1, lat2, lon2) {
        let R = 6371; // Radius of the earth in km
        let dLat = _deg2rad(lat2-lat1);  // deg2rad below
        let dLon = _deg2rad(lon2-lon1);
        let a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(_deg2rad(lat1)) * Math.cos(_deg2rad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        let d = R * c; // Distance in km
        return d;
    };

    let _deg2rad = function (deg) {
        return deg * (Math.PI/180)
    };

    return {
        init: init
    };

})();

$(document).ready(function(){
    startTime = new Date();
    main.init();
});