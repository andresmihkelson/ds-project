const puppeteer = require('puppeteer');

module.exports = {
    /** Fire the crawlers as processes **/
    fire_crawlers: function () {
        const base_url = 'http://localhost:8080/crawler_index.html?type=';
        (async() => {
            const browser = await puppeteer.launch();
            console.log(await browser.version());
            await browser.close();
        })();

        (async() => {
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            await page.goto(base_url + 'instagram', {waitUntil: 'networkidle2'});
            await page.pdf({path: 'processes_run_info/instagram.pdf', format: 'A4'});

            //await browser.close();
        })();


        (async() => {
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            await page.goto(base_url + 'facebook', {waitUntil: 'networkidle2'});
            await page.pdf({path: 'processes_run_info/faccebook.pdf', format: 'A4'});

            //await browser.close();
        })();

        (async() => {
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            await page.goto(base_url + 'foursquare', {waitUntil: 'networkidle2'});
            await page.pdf({path: 'processes_run_info/foursquare.pdf', format: 'A4'});

            //await browser.close();
        })();

        // (async() => {
        //     const browser = await puppeteer.launch();
        //     const page = await browser.newPage();
        //     await page.goto(base_url + 'twitter', {waitUntil: 'networkidle2'});
        //     await page.pdf({path: 'processes_run_info/twitter.pdf', format: 'A4'});
        //
        //     //await browser.close();
        // })();
    /* End (fire the crawlers as processes. */
    }
};