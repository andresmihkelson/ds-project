const WebSocket = require('ws');
const Peer = require('simple-peer');
const wrtc = require('wrtc');

// websocket connection to the signaling server
const ws = new WebSocket('ws://localhost:9500');

// Letting signaling server know of our existance
ws.on('open', function () {
    ws.send(JSON.stringify({
        type: 'REGISTER',
        name: 'standalone-node'
    }));
});

// Receiving signaling messages over websocket
ws.on('message', function (msgStr) {
    // P2P listener
    const p = new Peer({initiator: false, wrtc: wrtc});

    // Parse the received message and generate signaling response
    let msg = JSON.parse(msgStr);
    p.signal(msg.data);

    // Signaling response from this peer
    p.on('signal', function(data) {
        ws.send(JSON.stringify({
            'type': 'SIGNAL',
            'destination': msg.source,
            'source': msg.destination,
            'data': data
        }))
    });

    // When connected over WebRTC data channel, send hello message
    p.on('connect', function () {
        p.send(`Hi, from ${msg.destination} to ${msg.source}!`);
    });
});