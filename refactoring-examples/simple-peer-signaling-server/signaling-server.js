const WebSocket = require('ws');

// Map of peers - detecting and closing broken connections should be added
const peers = {};
const wss = new WebSocket.Server({port: 9500});

wss.on('connection', newConnection);

// Handle new webscoket conncetions
function newConnection(ws) {
    ws.on('message', incomingMessage);
}

// Handle incoming messages
function incomingMessage(msgStr) {
    msg = JSON.parse(msgStr);

    console.log(msg);

    // Peer registration (for crawlers for example)
    if (msg.type === 'REGISTER') {
        peers[msg.name] = this;

    // Signaling - used both ways
    } else if (msg.type === 'SIGNAL') {
        // Remember way back to the peer sending the message
        // (cannot assume at every node will send REGISTER)
        if (msg.source !== null) {
            peers[msg.source] = this
        }
        // Send signaling message
        peers[msg.destination].send(msgStr);
    }
}

