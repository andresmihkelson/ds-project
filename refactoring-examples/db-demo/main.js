// LokiJS - NoSQL DB for NodeJS and browser
let loki = require('lokijs');
// Fake data generator for demoing purposes
let faker = require('faker');

// DB configuration, with
db = new loki('demo.db', {
    autoload: true,
    autoloadCallback: initialiseDatabase,
    autosave: true,
    autosaveInterval: 5000
});


function initialiseDatabase() {
    // Get demo collection from DB
    let posts = db.getCollection('posts');

    // ... and create it, if DB didn't exist yet
    if (posts === null) {
        posts = db.addCollection('posts');
    }

    // start a simple demo
    startDemo(posts);
}

function startDemo(postsStorage) {
    // quering data
    console.log("Current posts: ", postsStorage.find());

    // inserting data
    postsStorage.insert({
        id: faker.random.uuid(),
        title: faker.lorem.words(3),
        content: faker.lorem.paragraphs(2)
    });

    // Process will hang, if DB is not closed
    db.close();
}

// For long-running command line processes implement proper signal handling
process.on('SIGINT', function() {
    db.close();
    process.exit(0);
});
