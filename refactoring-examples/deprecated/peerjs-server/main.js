let PeerServer = require('peer').PeerServer

let options = {
    host: 'localhost',
    port: '9000'
};

const server = PeerServer(options);

server.on('connection', function (id) {
    console.log(`Node connected: ${id}`)
});

server.on('disconnect', function (id) {
    console.log(`Node disconnected: ${id}`)
});

console.log(`PeerJS connection broker running, connect to ${options.host}:${options.port}...`);
