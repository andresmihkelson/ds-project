/*
 These hacks are from peerjs-nodejs package that uses Electron's WebRTC implementation.
 In this case wrtc library is used that has less dependencies.
  */

window = global;

XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

const wrtc = require('wrtc');

RTCPeerConnection = wrtc.RTCPeerConnection;
RTCSessionDescription = wrtc.RTCSessionDescription;
RTCIceCandidate = wrtc.RTCIceCandidate;

WebSocket = require('ws');
location = {
    protocol: 'http'
};

require('peerjs/lib/exports');

module.exports = function(id, options) {

    if (id && id.constructor === Object) {
        options = id;
        id = undefined;
    } else if (id) {
        // Ensure id is a string
        id = id.toString();
    }

    return new Peer(id, options);
};
