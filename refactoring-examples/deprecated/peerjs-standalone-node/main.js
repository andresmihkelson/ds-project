/*
!! This library does not work as it should. Use simple-peer demos instead !!
 */


// Custom PeerJS wrapper based on peerjs-nodejs - adds WebRTC and other APIs to NodeJS runtime
const peerjs = require('./peerjs.js');

// Connect to the broker
let peer = peerjs('standalone-node', {
    host: 'localhost',
    port: 9000
});

// When connection is opened
peer.on('open', function(id) {
    console.log(`Ready, our ID is '${id}'`)
});

// New connection handler
// WARNING: this currently crashes on every request except the first one
let handleNewConnection = function(conn) {
    conn.serialization = 'json';

    // When connection is opened, just respond
    conn.on('open', function() {
        conn.send("Hi, world!");
    });
};

// On new connection
peer.on('connection', handleNewConnection);
